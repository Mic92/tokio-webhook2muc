use std::net::SocketAddr;
use std::str::FromStr;
use std::sync::{Arc, Mutex};
use futures::{Stream, future::FutureResult, sync::mpsc::UnboundedSender};
use hyper::{Body, Request, Response, Server};
use hyper::header::HeaderValue;
use hyper::service::service_fn;
use hyper::rt::Future;
use gitlab::webhooks::WebHook;

static OK_STR: &'static [u8] = b"Ok";

#[derive(Debug, Clone)]
pub enum HeaderCheck {
    Name(String),
    Value(String, HeaderValue),
}

impl HeaderCheck {
    fn matches(&self, req: &Request<Body>) -> bool {
        match self {
            HeaderCheck::Name(name) =>
                req.headers().contains_key(name),
            HeaderCheck::Value(name, value) =>
                req.headers().get(name) == Some(value),
        }
    }
}

impl FromStr for HeaderCheck {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, ()> {
        let mut ss = s.splitn(2, ":");
        let key = ss.next().unwrap().to_owned();
        match ss.next() {
            None =>
                Ok(HeaderCheck::Name(key)),
            Some(value) => {
                match HeaderValue::from_str(value.trim_start()) {
                    Ok(value) =>
                        Ok(HeaderCheck::Value(key, value)),
                    Err(_) =>
                        panic!("Invalid HTTP header value: {:?}", value),
                }
            }
        }
    }
}

fn error_res<E: std::fmt::Debug, X>(e: E) -> Result<Response<Body>, X> {
    error!("error response: {:?}", e);

    let text = format!("{:?}", e);
    let res = Response::builder()
        .status(400)
        .body(Body::from(Vec::from(text.as_bytes())))
        .unwrap();
    Ok(res)
}

pub fn start(addr: SocketAddr, header_checks: Vec<HeaderCheck>, value_tx: UnboundedSender<WebHook>) -> Box<dyn Future<Item = (), Error = ()> + Send> {
    let value_tx = Arc::new(Mutex::new(value_tx));
    let header_checks = Arc::new(header_checks);
    let service = move || {
        let value_tx = value_tx.clone();
        let header_checks = header_checks.clone();
        service_fn(move |req: Request<Body>| {
            let mime_json = HeaderValue::from_static(&"application/json");
            debug!("request: {} {}", req.method(), req.uri());
            let is_json = req.headers().get("content-type")  == Some(&mime_json);
            let headers_ok = header_checks.iter().all(
                |header_check| header_check.matches(&req)
            );
            let value_tx = value_tx.clone();
            req.into_body().concat2()
                .and_then(move |body| {
                    let res = if !headers_ok {
                        Ok(Response::builder()
                           .status(400)
                           .body(Body::from(Vec::from(&b"Required headers are missing"[..])))
                           .unwrap()
                        )
                    } else if !is_json {
                        Ok(Response::builder()
                           .status(400)
                           .body(Body::from(Vec::from(&b"No JSON submitted"[..])))
                           .unwrap()
                        )
                    } else {
                        std::str::from_utf8(&*body)
                            .and_then(|body|
                                      serde_json::from_str(body)
                                      .map(|wh: WebHook| {
                                          trace!("body: {:?}", body);
                                          value_tx
                                              .lock()
                                              .unwrap()
                                              .unbounded_send(wh)
                                              .unwrap();
                                          Response::new(Body::from(OK_STR))
                                      })
                                      .or_else(error_res)
                            )
                            .or_else(error_res)
                    };
                    FutureResult::from(res)
                })
        })
    };
    let srv = Server::bind(&addr.into())
        .serve(service)
        .map_err(|e| error!("http server error: {}", e));
    info!("Listening on http://{}", addr);
    Box::new(srv)
}
